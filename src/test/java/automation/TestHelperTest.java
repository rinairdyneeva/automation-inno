package automation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class TestHelperTest {

    @org.junit.jupiter.api.Test
    void getRandomIntBetween1970andCurrentYear() {
        int currentYear = LocalDate.now().getYear();
        int beginYear = 1970;

        int lastResult = 0;
        int uniqueCount = 0;
        for (int i = 0; i < 10; i++) {
            int result = TestHelper.getRandomIntBetween1970andCurrentYear();
            assertTrue(result >= beginYear);
            assertTrue(result <= currentYear);
            if (result != lastResult) uniqueCount++;
            lastResult = result;
        }
        assertTrue(uniqueCount > 5);
    }

    @org.junit.jupiter.api.Test
    void getRandomLong() {
        long result1 = TestHelper.getRandomLong();
        long result2 = TestHelper.getRandomLong();
        long result3 = TestHelper.getRandomLong();

        assertNotEquals(result1, result2);
        assertNotEquals(result1, result3);
    }

    @org.junit.jupiter.api.Test
    void getRandomWords() {
        int wordCount = 3;
        String randomWords1 = TestHelper.getRandomWords(wordCount);
        String randomWords2 = TestHelper.getRandomWords(wordCount);
        String randomWords3 = TestHelper.getRandomWords(wordCount + 1);

        assertNotNull(randomWords1);
        assertNotNull(randomWords2);
        assertNotNull(randomWords3);

        assertNotEquals(randomWords1, randomWords2);
        assertNotEquals(randomWords1, randomWords3);

        assertTrue(Character.isUpperCase(randomWords1.charAt(0)));
        assertTrue(Character.isUpperCase(randomWords2.charAt(0)));
        assertTrue(Character.isUpperCase(randomWords3.charAt(0)));

        String[] words1 = randomWords1.split(" ");
        String[] words2 = randomWords2.split(" ");
        String[] words3 = randomWords3.split(" ");

        assertEquals(wordCount, words1.length);
        assertEquals(wordCount, words2.length);
        assertEquals(wordCount + 1, words3.length);

        assertTrue(Character.isUpperCase(words1[1].charAt(0)));
        assertTrue(Character.isUpperCase(words2[1].charAt(0)));
        assertTrue(Character.isUpperCase(words3[1].charAt(0)));

        assertTrue(Character.isUpperCase(words1[2].charAt(0)));
        assertTrue(Character.isUpperCase(words2[2].charAt(0)));
        assertTrue(Character.isUpperCase(words3[2].charAt(0)));

        assertTrue(Character.isUpperCase(words3[3].charAt(0)));
    }

    @ParameterizedTest
    @ValueSource(ints = {11, 20, 0, -1})
    void getRandomWordsWithException(int wordCount) {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> TestHelper.getRandomWords(wordCount));

        String expectedMessage = "Некорректное количество слов(1-20): " + wordCount;
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));

    }

    @ParameterizedTest
    @CsvSource(value = {
            "input_map.txt;::;12;key4;value4;missedKey",
            "input_map2.txt;::;6;key7;value7;key1"}, delimiter = ';')
    void readMapFromFile(String filename, String separator, int recordCount, String keyCheck, String valueExpected, String missedKey) {
        Map<String, String> records = TestHelper.readMapFromFile(filename, separator);

        assertEquals(recordCount, records.size());
        assertTrue(records.containsKey(keyCheck));
        assertEquals(valueExpected, records.get(keyCheck));
        assertTrue(!records.containsKey(missedKey));
    }
    @org.junit.jupiter.api.Test
    void readMapFromFileWithThrow() {
        Exception exception = assertThrows(RuntimeException.class,
                () -> TestHelper.readMapFromFile("missedFilename", "%"));

        String expectedMessage = "Ошибка чтения файла, проверьте правильность пути к файлу";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @ParameterizedTest
    @CsvSource(value = {
            "12/07/2001:dd/MM/yyyy:yyyy-dd-MM:2001-12-07",
            "12.07.2001:dd.MM.yyyy:yyyy:2001",
            "12.07.2001:dd.MM.yyyy:dd-MM:12-07",
            "12-07-2001:dd-MM-yyyy:yyyy.dd.MM:2001.12.07",
            "12072001:ddMMyyyy:-dd-:-12-",
            "12 07 2001:dd MM yyyy:dd.MM.yyyy:12.07.2001"}, delimiter = ':')
    void dateFormat(String date, String format, String targetFormat, String expected) {
        String result = TestHelper.dateFormat(date, format, targetFormat);
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void dateFormatWithException() {
        Exception exception = assertThrows(RuntimeException.class,
                () -> TestHelper.dateFormat("12/07/2001", "---", "---"));
        assertNotNull(exception);
    }

    @ParameterizedTest
    @ValueSource(doubles = {3.003, 0.0, -222.222, Double.POSITIVE_INFINITY})
    void stringToDouble(Double expected) {
        double result = TestHelper.stringToDouble(String.valueOf(expected));
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void stringToDoubleWithInvalidString() {
        double result = TestHelper.stringToDouble("@#sfds2333222");
        assertEquals(Double.POSITIVE_INFINITY, result);
    }
}
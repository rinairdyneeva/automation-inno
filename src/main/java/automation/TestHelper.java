package automation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class TestHelper {

    private TestHelper() {}

    public static int getRandomIntBetween1970andCurrentYear() {
        int currentYear = LocalDate.now().getYear();
        int randomInt = new Random().nextInt(currentYear - 1970);
        return 1970 + randomInt;
    }

    public static long getRandomLong() {
        return new Random().nextLong();
    }

    public static String getRandomWords(int wordCount) {
        if (wordCount > 10 || wordCount < 1)
            throw new IllegalArgumentException("Некорректное количество слов(1-20): " + wordCount);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < wordCount; i++)
            stringBuilder
                    .append(generatingRandomAlphabeticString())
                    .append(" ");
        return stringBuilder.toString();
    }

    public static Map<String, String> readMapFromFile(String filePath, String separator) {
        try {
            Map<String, String> lines = new HashMap<>();
            Files.readString(Path.of(filePath))
                    .lines()
                    .map(line -> line.split(separator))
                    .filter(tokens -> tokens.length > 1)
                    .forEach(tokens -> lines.put(tokens[0], tokens[1]));
            return lines;
        } catch (IOException exception) {
            throw new RuntimeException("Ошибка чтения файла, проверьте правильность пути к файлу");
        }
    }

    public static String dateFormat(String dateString, String dateFormat, String targetFormat) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateFormat);
        DateTimeFormatter targetFormatter = DateTimeFormatter.ofPattern(targetFormat);
        LocalDate localDate = LocalDate.parse(dateString, dateTimeFormatter);
        return targetFormatter.format(localDate);
    }

    public static Double stringToDouble(String doubleString) {
        try {
            return Double.parseDouble(doubleString);
        } catch (NumberFormatException ex) {
            return Double.POSITIVE_INFINITY;
        }
    }

    private static String generatingRandomAlphabeticString() {
        Random random = new Random();
        int targetStringLength = 3 + random.nextInt(4);
        char firstChar = Character.toUpperCase((char) (97 + random.nextInt(25)));
        return firstChar + random.ints('a', 'z' + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }


}
